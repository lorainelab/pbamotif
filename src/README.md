# Scripts that make code for binding site motif analysis

## Pipeline

* Use mergeModels.sh to merge overlapping gene models into gene regions. 
* Use makePromoterBed.py to make promoter regions bed file using output of mergeModels.sh
* Use countMotifs.py to count number of times sequences appears in promoter regions BED file, output of makePromoterBed.py

For example invocation, see doItAll.sh

## Requires

* 2bit file for rice from IGB Quickload.org 
* twobitreader modeul - see https://pythonhosted.org/twobitreader/

