#!/usr/bin/env python

# There were 108 regions associated with multiple gene ids.


import fileinput,argparse,sys

ex=\
"""Make promoter regions bed file using output from bedtools merge."""

sep='\t'
default_size=1000

def main(size=None,infile=None):
    multis=0
    lines = infile.readlines()
    for line in lines:
        toks=line.rstrip().split(sep)
        seqname=toks[0]
        start=toks[1]
        end=toks[2]
        name=toks[3]
        strand=toks[4]
        names={}
        name_toks=name.split(',')
        for tok in name_toks:
            gene_name=getGeneName(tok)
            names[gene_name]=1
        if len(names.keys())>1:
            sys.stderr.write("Overlapping genes: %s\n"%','.join(names.keys()))
            multis=multis+1
        else:
            if strand=='+':
                newstart=str(int(start)-size)
                newend=start
            else:
                newstart=end
                newend=str(int(end)+size)
            newline=[seqname,newstart,newend,names.keys()[0],'0',strand]
            newline=sep.join(newline)+'\n'
            sys.stdout.write(newline)
    sys.stderr.write("There were %i regions associated with multiple gene ids.\n"%multis)

# MSU7 ids start with LOC or look like ChrSy.fgenesh.mRNA
# Sy is from "Syngenta"         
def getGeneName(name):
    if name.startswith('LOC'):
        toks=name.split('.')
        to_return=toks[0]
    else:
        to_return=name
    return to_return


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=ex)
    parser.add_argument("-s","--size",help='size (bp) of promoter region (default %i)'%default_size,
                        dest="size",type=int,default=default_size)
    parser.add_argument("infile",help="name of file with gene regions",type=argparse.FileType('r'),
                        nargs='?',default=sys.stdin)
    args=parser.parse_args()
    main(size=args.size,infile=args.infile)

